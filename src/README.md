
Onboard your device onto the Internet of Things Cloud. You will get the following information.

1. Organization Id
2. Device Type ID
3. Device ID
4. Authetication Method
5. Authetication Token


sudo nano /etc/iot-mqtt/device.cfg

```	
#Sample Device configuration file...
org = yourOrganizationCode
type = iotsample-raspberrypi
id =b827eba84426
auth-method=token
auth-token = yourAuthToken
#End of Configuration file.	

Note: All the properties in the configuration file are mandatory.
		Currently, only token based authetication is supported. 
```
Save your file by pressing CTRL-X in Nano and follow the steps on the screen

Start the iot process. 

