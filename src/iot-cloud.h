/*
 * iot-cloud.h
 *
 */

#ifndef IOTCLOUD_H_
#define IOTCLOUD_H_

#include "iot.h"
#include <MQTTAsync.h>
#include <pktProc.h>
#include <eventThread.h>
#include <sensor.h>

//extern void initDb(void);
//extern void endDb(void);
extern MQTTAsync* openDbConnection(void);
extern void closeDbConnection(MQTTAsync *con);
extern void printErrAndExit(MQTTAsync *con, unsigned int lineNum);
extern void printErr(MQTTAsync *con, unsigned int lineNum);
extern unsigned int getErrno(MQTTAsync *con);
extern UINT8_t isPresent(MQTTAsync *con, char *cmd);
extern void createTables(MQTTAsync *con);
extern void initTableVersions(MQTTAsync *con);
extern UINT8_t updateNodeInfo(MQTTAsync *con, NodeInfo *nodeInfo_p);
extern UINT16_t readPushPeriod(MQTTAsync *con, UINT8_t *macAddr);
extern UINT8_t updateNodeSensors(MQTTAsync *con, UINT8_t *macAddr, SensorInfo *sensor_p);
extern UINT8_t updateSensorInfo(MQTTAsync *con, UINT8_t *macAddr, SensorInfo *sensor_p);
extern UINT8_t updatePanId(MQTTAsync *con, UINT16_t value);
extern UINT8_t updateNodeCount(MQTTAsync *con, UINT16_t value);
extern UINT8_t updateBand(MQTTAsync *con, UINT8_t* str);
extern UINT8_t updateNwChannel(MQTTAsync *con, UINT32_t value);
extern UINT16_t readChannelNumber(MQTTAsync *con);
extern UINT8_t updateModulation(MQTTAsync *con, UINT8_t* str);
extern UINT8_t updateRadioBaudRate(MQTTAsync *con, UINT32_t value);
extern UINT8_t updateCoordExtAddr(MQTTAsync *con, UINT8_t *macAddr);
extern UINT16_t readCoordUid(MQTTAsync *con);
extern char* readCoordExtAddr(MQTTAsync *con);
extern char* readNodeExtAddr(MQTTAsync *con, UINT16_t macShortAddr);
extern UINT8_t updateCoordTxPwr(MQTTAsync *con, UINT16_t value);
extern UINT8_t updateNodeAssocState(MQTTAsync *con, UINT8_t value);
extern UINT8_t readNodeAssocState(MQTTAsync *con);
extern UINT8_t updateNodeBuildDate(MQTTAsync *con, UINT8_t *macAddr, char *value_p);
extern UINT8_t updateNodeBuildTime(MQTTAsync *con, UINT8_t *macAddr, char *value_p);
extern UINT8_t addToNodeWhiteList(MQTTAsync *con, UINT8_t *macAddr);
extern UINT8_t insertEvent(MQTTAsync *con, UINT8_t *macAddr, const char *severity, const char *type, char *msg);
extern UINT8_t updateRssiAndCorrelation(MQTTAsync *con, UINT8_t *macAddr, SINT8_t rssi, UINT8_t corr);
extern UINT8_t updateSensorData(MQTTAsync *con, UINT8_t *macAddr, UINT8_t sensorId, UINT16_t value);
extern UINT8_t updateRouteInfo(MQTTAsync *con, UINT8_t *macAddr);
extern MQTTAsync_RES* readNodeWhiteList(MQTTAsync *con);
extern MQTTAsync_RES* readRule(MQTTAsync *con, UINT8_t *macAddr, UINT8_t sensorId);
extern UINT8_t updateRuleNotifiedTs(MQTTAsync *con, UINT16_t ruleUid);

#ifdef GUI_INTERFACE_DB
extern PendingAction* getFirstPendingAction(MQTTAsync *con);
extern UINT8_t deleteFirstPendingAction(MQTTAsync *con);
extern UINT8_t clearAllPendingActions(MQTTAsync *con);
#endif

#endif /* IOTCLOUD_H_ */
